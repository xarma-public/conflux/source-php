<?php

namespace Xarma\Conflux\Source;

use Xarma\Conflux\Source\Source\Config;

interface Factory
{
    /**
     * Gets a Source for the given Source Config and Destination
     *
     * @param Config $config
     * @param Sink $destination
     *
     * @return Source
     */
    public function get(Config $config, Sink $destination): Source;
}
