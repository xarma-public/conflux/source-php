<?php

namespace Xarma\Conflux\Source;

use Psr\Log\LoggerInterface;

interface Source
{
    /**
     * Ingest from this Source, sending to Sink
     *
     * @param Sink $sink
     * @param Options $options
     *
     * @return void
     */
    public function ingest(Sink $sink, Options $options): void;

    /**
     * @see \Psr\Log\LoggerAwareTrait
     */
    public function setLogger(LoggerInterface $logger): void;
}
