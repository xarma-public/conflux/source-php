<?php

namespace Xarma\Conflux\Source;

abstract class SourceObject implements \JsonSerializable
{
    public function __construct(\stdClass $data)
    {
        $this->fromData($data);
    }

    public function fromData(\stdClass $data)
    {
        foreach ($this->getMap() as $source => $destination) {
            if (property_exists($data, $source)) {
                $this->{$destination} = $data->{$source};
            }
        }
    }

    abstract public function getType(): string;

    abstract public function getSource(): string;

    abstract public function getId(): string;

    abstract public function getContent(): object;

    abstract protected function getMap(): array;

    protected function generateId($id, ?string $type = null, ?string $source = null)
    {
        $source = $source ?: $this->getSource();
        $type = $type ?: $this->getType();

        return strtr(
            strtolower(implode('.', [$source, $type, $id])),
            [' ' => '_', '&' => 'and']
        );
    }

    public function jsonSerialize(): mixed
    {
        $obj = (object)[
            'source' => $this->getSource(),
            'type' => $this->getType(),
            'id' => $this->generateId($this->getId()),
            'generated' => time(),
            'content' => $this->getContent()
        ];

        return $obj;
    }
}
