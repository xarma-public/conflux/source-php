<?php

namespace Xarma\Conflux\Source;

interface Sink
{
    /**
     * Sends a SourceObject via the Sink
     *
     * @param SourceObject $message
     *
     * @return bool True if sent; false otherwise.
     */
    public function send(SourceObject $message): bool;
}
