<?php

namespace Xarma\Conflux\Source;

interface Options
{
    /**
     * Gets a Source's options by key
     *
     * @param string $key
     * @param mixed $default If $key does not exist, return this value
     *
     * @return mixed
     */
    public function get(string $key, mixed $default = null): mixed;

    /**
     * Returns true if $key exists in this object, otherwise false
     *
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool;
}
