# Conflux Source

A collection of interfaces used to define data sources for [Xarma Conflux](https://xarma.co/conflux).

## Usage

```bash
$ composer require xarma/conflux-source
```